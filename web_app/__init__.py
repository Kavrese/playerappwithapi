from flask_moment import Moment
from . import terminal

moment = Moment()

app = terminal.create_app(moment=moment)
