import logging
from flask import Flask


def create_app(moment):
    app = Flask(__name__)
    moment.init_app(app)
    from .api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')
    app.logger.setLevel(logging.INFO)
    app.logger.info('App startup')
    return app
