from functools import wraps

from sqlalchemy import create_engine
from flask import request
from web_app.api.errors import *

HOST_DB = "localhost"
PORT_DP = "5432"
NAME_DB = "postgres"
USER_DB = "bob"
PASSWORD_DB = "admin"


def connect_to_db():
    conn = create_engine(f'postgresql+psycopg2://{USER_DB}:{PASSWORD_DB}@{HOST_DB}:{PORT_DP}/{NAME_DB}').connect()
    return conn


def convert_dict_to_str(dict_):
    str_dict = str(dict_).replace("'", "\"")
    return str_dict


def convert_results_to_dict(result):
    return [i.to_dict() for _, i in result.iterrows()][0] if not result.empty else {}


def convert_results_to_list_dict(result):
    return [i.to_dict() for _, i in result.iterrows()] if not result.empty else []


# Проверка параметров в запросе
# params - словарь (параметр: "param" или "body") обязательных параметров
def required_params(params: dict):
    def inner(func):
        def wrapper(*args, **kwargs):

            check_body = not all([params[i] == "param" for i in params])
            check_param = not all([params[i] == "body" for i in params])

            if check_body and request.form is None:
                return error_response(400, f'body not found in request')

            if check_param:
                need_params = [i for i in params if params[i] == "param"]
                no_exist_params = [param for param in need_params if param not in request.args]
                if len(no_exist_params):
                    return error_response(400,
                                          f"{', '.join(no_exist_params)} not found in request params")
            if check_body:
                need_body_params = [i for i in params if params[i] == "body"]
                no_exist_body_params = [param for param in need_body_params if param not in request.get_json()]
                if len(no_exist_body_params):
                    return error_response(400,
                                          f"{', '.join(no_exist_body_params)} not found in request body params")

            return func(*args, **kwargs)

        wrapper.__name__ = func.__name__
        return wrapper

    return inner


def check_exist_file(key: str = "file"):
    def inner(func):
        def wrapper(*args, **kwargs):
            res = key in request.files
            if not res:
                return error_response(404, "File in request not found")
            return func(*args, **kwargs)

        wrapper.__name__ = func.__name__
        return wrapper

    return inner


def mult_threading(func):
    """Декоратор для запуска функции в отдельном потоке"""

    @wraps(func)
    def wrapper(*args_, **kwargs_):
        import threading
        func_thread = threading.Thread(target=func,
                                       args=tuple(args_),
                                       kwargs=kwargs_)
        func_thread.start()
        return func_thread

    return wrapper
