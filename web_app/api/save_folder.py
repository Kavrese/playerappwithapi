import os

from werkzeug.datastructures import FileStorage


class SaveFolder:
    def __init__(self):
        self.path = "/home/artemii/PycharmProjects/SaveFileAPI"

    def get_files_titles(self):
        files = os.listdir(self.path)
        return files

    def get_files_with_filter(self, filter_type: list):
        all_files = self.get_files_titles()
        return [file for file in all_files if file[len(file) - file[::-1].index("."):] in filter_type]

    def delete_file(self, title):
        try:
            os.remove(self.path + "/" + title)
            return True
        except:
            return False

    def save_file(self, title: str, file: FileStorage):
        try:
            file.save(self.path + "/" + title)
            return True
        except:
            return False

    def check_exist_file(self, title: str):
        return title in self.get_files_titles()
