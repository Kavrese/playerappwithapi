from flask import jsonify, send_from_directory
from . import bp, save_folder
from ..utils import *


@bp.route('/files', methods=['GET'])
def get_files():
    filter_files: list = request.args.get("filter", default="", type=str).strip(", ")
    files = save_folder.get_files_with_filter([filter_files])
    return jsonify(files)


@bp.route("/delete", methods=["DELETE"])
@required_params({"title": "param"})
def delete_file():
    title = request.args.get("title")
    if not save_folder.check_exist_file(title):
        return not_found_this_file()
    answer = save_folder.delete_file(title)
    return jsonify({"success": answer})


@bp.route("/save", methods=["POST"])
@check_exist_file()
def save_file():
    file = request.files['file']
    title = request.args.get("title", default=file.filename)
    answer = save_folder.save_file(title, file)
    return jsonify({"success": answer})


@bp.route("/load", methods=["GET"])
@required_params({"title": "param"})
def load_file():
    title = request.args.get("title")
    return send_from_directory(save_folder.path, title) if save_folder.check_exist_file(title) else not_found_this_file()

