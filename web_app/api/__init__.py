from flask import Blueprint
from .save_folder import SaveFolder

bp = Blueprint('api', __name__)
save_folder = SaveFolder()

from . import api